package by.epamlab.bikes;

import by.epamlab.seats.Seat;

/**
 * Class describes model of RoadBike
 *   - extends - {@link Bike}
 * @author – Darya Horb.
 */

public class TandemBike extends Bike {

    /**
     * This field contains information about second seat
     */
    private Seat seat2;
    /**
     * This field contains information about second cadence
     */
    private int cadence2;
    /**
     * This field contains information about average gear of tandem bike
     */
    private static final int STANDART_GEAR = 8;

    public TandemBike() {
        super();
        this.seat2 = new Seat();
    }

    /**
     * Ride method set current speed of TandemBike
     */

    public TandemBike(int cadence, int wheelCircumference, int cadence2) {
        super(cadence, wheelCircumference);
        this.cadence2 = cadence2;
        seat2 = new Seat();

    }

    public void ride() {
//        logger.info("Tandem bike rides");
        setCadence(avgCadence());
        setSpeed(getBasicSpeed() * STANDART_GEAR);
    }

    /**
     * Method calculates avgCadence
     */

    public int avgCadence() {

        return (getCadence() + cadence2) / 2;

    }

    @Override
    public String toString() {

        StringBuilder outB = new StringBuilder();
        outB.append(super.toString());
        outB.append(";cadence2=");
        outB.append(cadence2);
        outB.append(seat2);
        return outB.toString();
    }

    /**
     * Stop method set speed = 0 and cadence = 0
     */

    public void stop() {
        setSpeed(0);
        setCadence(0);
        cadence2 = 0;
 //       logger.info("Tandem bike stoppped");

    }

}
