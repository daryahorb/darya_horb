package by.epamlab.bikes;

/*import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;*/

import by.epamlab.iface.Rideable;
import by.epamlab.seats.Seat;

/**
 * Class describes model of Bike
 *  - implements {@link Rideable}.
 * @author – Darya Horb.
 */
public abstract class Bike implements Rideable {

    /**
     * This field contains information about speed
     */
    private int speed;
    /**
     * This field contains information about cadence
     */
    private int cadence;
    /**
     * This field contains information about wheelCircumference
     */
    private int wheelCircumference;
    /**
     * This field contains information about seat
     */
    private Seat seat;
    /**
     * This field contains information about value which translates millimeters to
     * kilometers
     */
    private static final double CONST = 0.00006;
    /**
     * logger
     */
//    static final Logger logger = LogManager.getRootLogger();

    public Bike(int cadence, int wheelCircumference) {
        this.cadence = cadence;
        this.wheelCircumference = wheelCircumference;
        this.seat = new Seat();
    }

    public Bike() {
        speed = 0;
        cadence = 0;
        wheelCircumference = 0;
        this.seat = new Seat();
    }

    public int getSpeed() {
        return speed;
    }

    public void setSpeed(int speed) {
        this.speed = speed;
    }

    public int getCadence() {
        return cadence;
    }

    public void setCadence(int cadence) {
        this.cadence = cadence;
    }
    
    /**
     * Ride method set current speed of Bike
     */
    public void ride() {
//        logger.info("Bike rides");
        setSpeed(getBasicSpeed());
    }

    /**
     * Method get basic speed without gear
     * 
     * @return – return basic speed.
     */
    public int getBasicSpeed() {
        return (int) (cadence * wheelCircumference * CONST);
    }

    /**
     * Method change height of seat
     * 
     * @param value – height
     */
    public void changeHeight(int value) {
        seat.changeHeight(value);
    }
    
    /**
     * Stop method set speed = 0 and cadence = 0
     */

    public void stop() {
        setSpeed(0);
        setCadence(0);
//        logger.info("Bike stoppped");

    }

    @Override
    public String toString() {
        StringBuilder outB = new StringBuilder();
        outB.append("speed=");
        outB.append(speed);
        outB.append(";cadence=");
        outB.append(cadence);
        outB.append(";wheelCircumference=");
        outB.append(wheelCircumference);
        outB.append(seat);
        return outB.toString();
    }

}
