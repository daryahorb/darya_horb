package by.epamlab.bikes;

/**
 * Class describes model of RoadBike
 *   - extends - {@link Bike}
 * @author – Darya Horb.
 */

public class RoadBike extends Bike {

    /**
     * This field contains information about cargo
     */
    public int cargo;
    /**
     * This field contains information about average gear of road bike
     */
    private static final int STANDART_GEAR = 8;

    public RoadBike() {
        super();
        cargo = 0;
    }

    public RoadBike(int cadence, int wheelCircumference, int cargo) {
        super(cadence, wheelCircumference);
        this.cargo = cargo;

    }

    /**
     * Ride method set current speed of RoadBike
     */

    public void ride() {
 //       logger.info("Road bike rides");
        setSpeed(getBasicSpeed() * STANDART_GEAR - cargo);
    }

    @Override
    public String toString() {
        StringBuilder outB = new StringBuilder();
        outB.append(super.toString());
        outB.append(";cargo=");
        outB.append(cargo);
        return outB.toString();
    }

    /**
     * Stop method set speed = 0 and cadence = 0
     */

    public void stop() {
        setSpeed(0);
        setCadence(0);
 //      logger.info("Road bike stoppped");

    }

}
