package by.epamlab.bikes;

/**
 * Class describes model of MountainBike
 *  - extends - {@link Bike}
 * @author – Darya Horb.
 */

public class MountainBike extends Bike {

    /**
     * This field contains information about gear
     */
    private int gear = 1;

    public MountainBike() {
        super();
        gear = 1;
    }

    public MountainBike(int cadence, int wheelCircumference, int gear) {
        super(cadence, wheelCircumference);
        this.gear = gear;
    }

    /**
     * Ride method set current speed of MountainBike
     */
    public void ride() {
//        logger.info("Mountain bike rides");
        setSpeed(getBasicSpeed() * gear);
    }

    @Override
    public String toString() {
        StringBuilder outB = new StringBuilder();
        outB.append(super.toString());
        outB.append(";gear=");
        outB.append(gear);
        return outB.toString();

    }

    /**
     * Stop method set speed = 0 and cadence = 0
     */

    public void stop() {
        setSpeed(0);
        setCadence(0);
 //       logger.info("Mountain bike stoppped");

    }

}
