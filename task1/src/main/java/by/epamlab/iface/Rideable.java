package by.epamlab.iface;

/**
 * Interface for bikes
 * @author – Darya Horb.
 */

public interface Rideable {

    void ride();

    void stop();

}
