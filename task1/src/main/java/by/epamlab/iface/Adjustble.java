package by.epamlab.iface;

/**
 * Interface for seat adjustment
 * @author – Darya Horb.
 */

public interface Adjustble {
    void changeHeight(int value);
}
