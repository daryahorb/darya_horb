package by.epamlab;

import by.epamlab.bikes.Bike;
import by.epamlab.bikes.MountainBike;
import by.epamlab.bikes.RoadBike;
import by.epamlab.bikes.TandemBike;

/**
 * Class Runner
 * 
 * @author – Darya Horb.
 */

public class Runner {

//	private static final Logger logger = LogManager.getRootLogger();

	public static void main(String[] args) {
		Bike bike = new MountainBike(55, 2004, 10);
		bike.changeHeight(55);
		bike.ride();
		Bike bike1 = new RoadBike(55, 2006, 5);
		bike1.ride();
		Bike bike2 = new TandemBike(55, 2007, 43);
		bike2.ride();
		/*logger.info(bike);
		logger.info(bike1);
		logger.info(bike2);*/
		bike1.stop();
		bike2.stop();
		bike.stop();
		/*logger.info(bike);
		logger.info(bike1);
		logger.info(bike2);*/

		System.out.println("Hello world");

	}

}
