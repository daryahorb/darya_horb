package by.epamlab.seats;

import by.epamlab.iface.Adjustble;

/**
 * Class describes model of Seat
 *  - implements – {@link Adjustble}.
 * @author – Darya Horb.
 */

public class Seat implements Adjustble {

    /**
     * This field contains information about seatHeight
     */
    public int seatHeight;

    public Seat() {
        seatHeight = 70;
    }

    @Override
    public String toString() {
        StringBuilder outB = new StringBuilder();
        outB.append(";seatHeight=");
        outB.append(seatHeight);
        return outB.toString();
    }

    /**
     * Method change height of seat
     * 
     * @param value – height
     */

    public void changeHeight(int value) {

        seatHeight = value;

    }

}
